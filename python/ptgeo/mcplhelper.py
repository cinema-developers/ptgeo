################################################################################
##                                                                            ##
##  This file is part of Prompt (see https://gitlab.com/xxcai1/Prompt)        ##
##                                                                            ##
##  Copyright 2021-2024 Prompt developers                                     ##
##                                                                            ##
##  Licensed under the Apache License, Version 2.0 (the "License");           ##
##  you may not use this file except in compliance with the License.          ##
##  You may obtain a copy of the License at                                   ##
##                                                                            ##
##      http://www.apache.org/licenses/LICENSE-2.0                            ##
##                                                                            ##
##  Unless required by applicable law or agreed to in writing, software       ##
##  distributed under the License is distributed on an "AS IS" BASIS,         ##
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  ##
##  See the License for the specific language governing permissions and       ##
##  limitations under the License.                                            ##
##                                                                            ##
################################################################################


import numpy as np
from scipy.spatial import KDTree
import mcpl
from Cinema.Prompt.geo import Transformation3D

class ChannelEventClassifier:
    def __init__(self, dictfile) -> None:

        # dictfile contains pmclfn, names, centres, rations

        self.tree = KDTree(centres, leaf_size=2)    
        self.ratations = rations # for rotational matrix  

        rot = Transformation3D(x=0., y=0., z=0., rot_z=0., rot_new_x=0., rot_new_z=0.)
        rotivt = rot.inv()
        rotivt.transformInplace()

    def query(self, x):
        dist, idx = self.tree.query(np.atleast_2d(x), k=1, workers=8)
        return idx


    # print(tree.query(X[:1], k=1, return_distance=False))          
